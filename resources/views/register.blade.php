<!DOCTYPE html>
<html>
<head>
    <title>Intro Laravel</title>
</head>
    <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First Name:</label><br><br>
        <input type="text" id="firstname" name="depan"><br><br>
        <label for="lastname">Last Name:</label><br><br>
        <input type="text" id="lastname" name="belakang"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" id="Male" name="sex">Male<br>
        <input type="radio" id="Female" name="sex">Female<br>
        <input type="radio" id="Other" name="sex">Other<br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select name="negara">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label for="spoken">Language Spoken:</label><br><br>
        <input type="checkbox" name="skill">Bahasa Indonesia<br>
        <input type="checkbox" name="skill">English<br>
        <input type="checkbox" name="skill">Other<br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br>
        <input type="submit" value="Kirim!">
    </form>
</body>
</html>